FROM maven:3.8.5-jdk-11-slim as build
# AS build
WORKDIR /workspace/source
RUN mvn package

FROM openjdk:11-jre-slim
COPY --from=build /workspace/source/target /home/app
EXPOSE 8080
ENTRYPOINT ["java","-jar","/home/app/react-and-spring-data-rest-0.0.1-SNAPSHOT.jar"]
